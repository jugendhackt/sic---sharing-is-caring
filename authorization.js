
const db = require("./databaseWrapper")

/** 
 * Manages authorization
 * @constructor db: Database wrapper
*/
module.exports.Authorizer = class {

    constructor(db){
        this._db = db;
    }

    login(username, password){
        if(username != null && password != null){
            return db.login(this._db, username, password)
        }
        return new Promise(resolve => resolve({valid: false, error: "Missing arguments"}))
    }

    // Can be attached to any express request as middleware
    express(req, res, next) {
        if(!req.get("Authorization")){
            res.status(401).send({
                error: "Unauthorized"
            });
            return;
        }
        
        this.validateToken(req.get("Authorization")).then(result => {
            if(result.valid){
                next();
            }else{
                res.status(401).send({
                    error: "Unauthorized"
                })
            }
        })
    }

    validateToken(token){
        return Promise( function(resolve, reject) {
            resolve({
                valid: true        // TODO: Implement verification logic       
            })
        })
    }
}