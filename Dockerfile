FROM node:10
EXPOSE 8888
WORKDIR /usr/src/app
ADD package.json package.json
ADD backend.js backend.js
ADD authorization.js authorization.js
ADD databaseWrapper.js databaseWrapper.js
ADD osmUtil.js osmUtil.js

ADD data.dat.json data.dat.json

ADD user.json user.json

ADD images/* images/

RUN "ls"

RUN npm install

CMD ["node", "backend.js"]