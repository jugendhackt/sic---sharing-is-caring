const fetch = require('node-fetch');
const NOMINATIM_REVERSE_API_BASE_URL = 'https://nominatim.openstreetmap.org/reverse?lat=<lat>&lon=<lon>&osm_type=W&format=json'

class OSMUtil {
    static async getStreetName(lat, lon) {
        lat = Number.parseFloat(lat);
        lon = Number.parseFloat(lon);

        let apiResponse = await (fetch(NOMINATIM_REVERSE_API_BASE_URL.replace('<lat>', lat).replace('<lon>', lon))
            .then(res => res.json()));

        console.log(apiResponse.display_name);

        return apiResponse.display_name;
    }
}

module.exports = OSMUtil;